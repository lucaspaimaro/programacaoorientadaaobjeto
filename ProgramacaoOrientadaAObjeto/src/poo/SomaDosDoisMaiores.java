package poo;

import java.util.Scanner;

public class SomaDosDoisMaiores {

    static Scanner tecla = new Scanner(System.in);

	public static void main (String[] args) {
		 int op;
	        do {                
	            System.out.println("*** MENU PRINCIPAL ***");
	            System.out.println("1-Calcule a soma dos dois maiores valores: ");
	            System.out.println("2-Sair");
	            System.out.println("Digite sua op��o: ");
	            op = tecla.nextInt(); 
	            switch(op){
	                case 1: somaDoisMaiores(); break;
	                case 2:  break;

	            }
	        } while (op!=2);    
	}
	public static void somaDoisMaiores() {
		int[] num = new int[3];

		for(int i = 0; i<= 2; i++) {
			System.out.println("Digite o " + i+1 + " n�mero: ");
			num[i] = tecla.nextInt();
		}

		int total = 0;
		if(num[0] > num[1] && num[1] > num[2]){
			total = num[0] + num[1];
		}else if(num[2] > num[1] && num[1] > num[0]) {
			total = num[2] + num[1];
		}else {
			total =  num[0] + num[2];
		}
		System.out.println("A soma dos valores �: " + total);
	}

}

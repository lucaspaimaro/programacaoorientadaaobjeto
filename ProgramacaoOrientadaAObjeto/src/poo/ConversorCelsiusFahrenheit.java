package poo;

import java.util.Scanner;

public class ConversorCelsiusFahrenheit {
	
    static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Converter temperatura de Celsius para Fahrenheit");
            System.out.println("2-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: converterCelsiusParaFahrenheit(); break;
                case 2:  break;

            }
        } while (op!=2);       
    }

	private static void converterCelsiusParaFahrenheit() {
		
		System.out.println("Digite uma temperatura em celsius: ");
		
		int celsius, fahrenheit;
		celsius = tecla.nextInt();
		
		fahrenheit = celsius + 32;
		
		System.out.println("A temperatura " + celsius + "� em celsius, cooresponde a " + fahrenheit + "� em fahrenheit.");
	}
	
	public int conversor(int num) {
		
		num += 32;
		
		return num;
		
	}
}

package poo;

import java.util.Scanner;

public class ParOuImpar {

    static Scanner tecla = new Scanner(System.in);

	public static void main (String[] args) {
		 int op;
	        do {                
	            System.out.println("*** MENU PRINCIPAL ***");
	            System.out.println("1-Par ou �mpar: ");
	            System.out.println("2-Sair");
	            System.out.println("Digite sua op��o: ");
	            op = tecla.nextInt(); 
	            switch(op){
	                case 1: parOuImpar(); break;
	                case 2:  break;

	            }
	        } while (op!=2);    
	}
	public static void parOuImpar() {
		int num = 0;

		System.out.println("Digite um n�mero: ");

		num = tecla.nextInt();
		if(num % 2 == 0){
			System.out.println("O n�mero " + num + " � par.");
		}else {
			System.out.println("O n�mero " + num + " � �mpar.");
		}		
	}

}
